import { Component, OnInit } from '@angular/core';
import { Empleado } from './empleado';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {
  public titulo = 'Componente empleados';
  public trabajador: Empleado;
  public empleados: Array<Empleado>;
  public trabajadorExterno: boolean;
  public color: string;
  public color_seleccionado: string;
  constructor() {
    this.trabajador = new Empleado('Julio Guzman', 28, 'Desarrollador', true);
    this.empleados = [
      new Empleado('Cesar Guzman', 28, 'Programador', false),
      new Empleado('Felipe Guzman', 28, 'Desarrollador', true),
      new Empleado('Skailer Rivas', 28, 'Politologo', false)
    ];
    this.trabajadorExterno = true;
    this.color = 'green';
    this.color_seleccionado = 'blue';
  }

  ngOnInit() {
  }

  cambiarExterno(value) {
    this.trabajadorExterno = value;
  }

}
