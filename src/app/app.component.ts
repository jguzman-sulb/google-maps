import { Component, ViewChild, ElementRef } from '@angular/core';


import { } from '@types/googlemaps';
interface markers {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  map: google.maps.Map;
  lat:number = 39;
  lng:number = -95;
  latswBound = 23.2;
  lngswBound = -120.5;
  latneBound = 53.8;
  lngneBound = -70.5;
  overlay;

  marker;

  poly: google.maps.Polyline;
  path;

  
  USGSOverlay = class extends google.maps.OverlayView {
    bounds_: any;
    image_: any;
    map_: any;
    div_: any;
    constructor(bounds, image, private map) {
        super();
        // Initialize all properties.
        this.bounds_ = bounds;
        this.image_ = image;
        this.map_ = map;
        // Define a property to hold the image's div. We'll
        // actually create this div upon receipt of the onAdd()
        // method so we'll leave it null for now.
        this.div_ = null;
        // Explicitly call setMap on this overlay.
        this.setMap(map);
    }
    /**
     * onAdd is called when the map's panes are ready and the overlay has been
     * added to the map.
     */
    onAdd() {
        const div = document.createElement('div');
        div.style.borderStyle = 'none';
        div.style.borderWidth = '0px';
        div.style.position = 'absolute';
        // Create the img element and attach it to the div.
        const img = document.createElement('img');
        img.src = this.image_;
        img.style.width = '100%';
        img.style.height = '100%';
        img.style.position = 'absolute';
        div.appendChild(img);
        this.div_ = div;
        // Add the element to the "overlayLayer" pane.
        const panes = this.getPanes();
        panes.overlayLayer.appendChild(div);
    };
    draw() {
        // We use the south-west and north-east
        // coordinates of the overlay to peg it to the correct position and size.
        // To do this, we need to retrieve the projection from the overlay.
        const overlayProjection = this.getProjection();
        // Retrieve the south-west and north-east coordinates of this overlay
        // in LatLngs and convert them to pixel coordinates.
        // We'll use these coordinates to resize the div.
        const sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
        const ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());
        // Resize the image's div to fit the indicated dimensions.
        const div = this.div_;
        div.style.borderWidth = '2px';
        div.style.borderStyle = '2px solid black';
        div.style.left = sw.x + 'px';
        div.style.top = ne.y + 'px';
        div.style.width = (ne.x - sw.x) + 'px';
        div.style.height = (sw.y - ne.y) + 'px';
    };
    // The onRemove() method will be called automatically from the API if
    // we ever set the overlay's map property to 'null'.
    onRemove() {
        this.div_.parentNode.removeChild(this.div_);
        //this.div_ = null;
    };
};
  @ViewChild('gmap') gmapElement: any;

  ngOnInit() {
    const mapProp = {
      center: new google.maps.LatLng(this.lat, this.lng),
      zoom: 5,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      backgroundColor: '#FFF',
      disableDefaultUI: true,
      draggable: false,
      scaleControl: false,
      scrollwheel: false,

      
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    this.poly = new google.maps.Polyline({
      strokeColor: '#000000',
      strokeOpacity: 1.0,
      strokeWeight: 3
    });
    this.poly.setMap(this.map);
    



//23.2, -120.5
//53.8, -70.5
    
    this.codeAddress('Florida');
    this.codeAddress('Arizona');
  }

  addMap() {
    console.log(this.latswBound);
    const swBound = new google.maps.LatLng(this.latswBound, this.lngswBound);
    const neBound = new google.maps.LatLng(this.latneBound, this.lngneBound);
    const bounds = new google.maps.LatLngBounds(swBound, neBound);

    const srcImage = 'http://localhost/mapa.svg';
    this.overlay = new this.USGSOverlay(bounds, srcImage, this.map);
    
  }
  delMap() {
    //this.overlay.div_ = null;
    this.overlay.onRemove();
    this.overlay = null;
  }

  addLatLng($event) {
    console.log(this.poly);
    this.path = this.poly.getPath();

    // Because path is an MVCArray, we can simply append a new coordinate
    // and it will automatically appear.
    this.path.push($event.latLng);

    // Add a new marker at the new plotted point on the polyline.
    this.marker = new google.maps.Marker({
      position: $event.latLng,
      title: '#' + this.path.getLength(),
      map: this.map
    });
    console.log(this.marker);
  }
  codeAddress(address) {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        this.marker = new google.maps.Marker({
            map: this.map,
            position: results[0].geometry.location,
            label: 'Prueba',
            draggable: true,
            title: 'Hola',
        });
      }
    });
  }

  
   
  
 


}
