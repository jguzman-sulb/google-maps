import { Component, OnInit } from '@angular/core';
import { Fruta } from './fruta';

@Component({
  selector: 'app-fruta',
  templateUrl: './fruta.component.html',
  styleUrls: ['./fruta.component.css']
})
export class FrutaComponent implements OnInit {
  public name: string = 'Hola';

  public frutas: Array<Fruta>;

  public fruta:Fruta;
  constructor() {
    this.fruta = new Fruta('Mango',2,true);
    this.frutas = [
      new Fruta('Mango', 2,true),
      new Fruta('Tamarindo',1,true),
      new Fruta('Naranja',2,false)
    ];
  }

  ngOnInit() {

    console.log(this.frutas);
  }

}
